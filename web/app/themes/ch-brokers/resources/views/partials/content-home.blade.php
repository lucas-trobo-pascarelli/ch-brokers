<section class="inst-hero" style="width: 100% !important;">

    <div class="container-flex">
        <div class="overlay"></div>

        <div class="d-flex align-items-center" style="background-image:url('@asset("images/img-home.jpg")');
            background-position: center; background-size: cover; height:400px;">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7">
                        <div class="hero-text">
                            <h1 class="title-primary hero">TUS BIENES
                                SEGUROS,<br>
                                VOS TRANQUILO </h1>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        @include('partials.index-login')
                    </div>
                </div>
            </div>
        </div>

    </div>

</section>