<header class="banner">
  <div class="container pl-0 pt-0">
    <nav class="navbar navbar-expand-lg justify-content-between">
      <a class="navbar-brand" href="{{ home_url('/') }}">
        <img src="@asset('images/logo.png')" alt="">        
      </a>
      <button type="button" class="navbar-toggle collapsed" id="navbar-toggle" data-toggle="collapse" data-target="#primary-navigation" aria-expanded="false">
          <span></span>
          <span></span>
          <span></span>
      </button>
      @if (has_nav_menu('primary_navigation'))
        {!! wp_nav_menu($primarymenu) !!}
      @endif
    </nav>
  </div>
</header>
